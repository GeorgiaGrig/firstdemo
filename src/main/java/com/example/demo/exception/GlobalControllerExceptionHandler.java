package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalControllerExceptionHandler {


    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Customer Not Found")
    @ExceptionHandler(CustomerNotFoundException.class)
    public void handleUserNotFoundException(CustomerNotFoundException e) {
    }
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Order Not Found")
    @ExceptionHandler(OrderNotFoundException.class)
    public void handleUserNotFoundException(OrderNotFoundException e) {
    }
}