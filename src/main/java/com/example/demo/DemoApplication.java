package com.example.demo;

import com.example.demo.config.CustomerConfig;
import com.example.demo.config.MongoConfig;
import com.example.demo.config.OrderConfig;

import com.example.demo.model.entity.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({CustomerConfig.class, OrderConfig.class, MongoConfig.class})
public class DemoApplication  {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}

}


