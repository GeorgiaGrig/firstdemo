package com.example.demo.controller;

import com.example.demo.model.DTO.OrderDTO;
import com.example.demo.model.entity.Order;
import com.example.demo.service.OrderService;
import ma.glasnost.orika.*;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderControllerImpl implements  OrderController{

    private final OrderService orderService;
    private MapperFacade mapperFacade;

    @Autowired
    public OrderControllerImpl(OrderService orderService) {
        this.orderService = orderService;
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        mapperFacade = factory.getMapperFacade();
    }

    @Override
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public List<OrderDTO> getOrdersByCustomerId(@RequestParam String id) {
        return mapperFacade.mapAsList(orderService.getOrderByCustomerId(id),OrderDTO.class);
    }

    @Override
    @RequestMapping(value = "/order", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
    public OrderDTO addOrder(@RequestBody OrderDTO orderDTO) {
        return mapperFacade.map(
                orderService.addOrder(
                        mapperFacade.map(orderDTO,Order.class)),
                OrderDTO.class);
    }

    @Override
    @RequestMapping(value = "/order", method = RequestMethod.DELETE)
    public OrderDTO deleteOrder(@RequestParam String id) {
        return mapperFacade.map(
                orderService.deleteOrder(id),
                OrderDTO.class);
    }

    @Override
    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public OrderDTO getOrderById(@RequestParam String id) {
        return mapperFacade.map(orderService.getOrderById(id), OrderDTO.class);
    }
}

