package com.example.demo.controller;

import com.example.demo.model.DTO.OrderDTO;

import java.util.List;

public interface OrderController {

    List<OrderDTO> getOrdersByCustomerId(String id);

    OrderDTO addOrder(OrderDTO orderDTO);

    OrderDTO deleteOrder(String id);

    OrderDTO getOrderById(String id);
}
