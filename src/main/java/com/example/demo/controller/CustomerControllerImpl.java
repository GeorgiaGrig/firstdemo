package com.example.demo.controller;

import com.example.demo.model.DTO.CustomerDTO;
import com.example.demo.model.entity.Customer;
import com.example.demo.service.CustomerService;
import ma.glasnost.orika.*;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
public class CustomerControllerImpl implements CustomerController{

    private final CustomerService customerService;
    private MapperFacade mapperFacade;

    @Autowired
    public CustomerControllerImpl(CustomerService customerService) {
        this.customerService = customerService;
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        mapperFacade = factory.getMapperFacade();

    }

    @Override
    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public CustomerDTO getCustomerByID(@NotNull String id){

        return mapperFacade.map(customerService.getCustomerById(id), CustomerDTO.class);
    }

    @Override
    @RequestMapping(value = "/customer", method = RequestMethod.PUT)
    public CustomerDTO updateCustomer(@Valid CustomerDTO customerDTO) {
        return mapperFacade.map(
                customerService.updateCustomer(
                        mapperFacade.map(customerDTO,
                                Customer.class)),
                CustomerDTO.class);
    }

    @Override
    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public CustomerDTO addCustomer(CustomerDTO customerDTO) {
        return mapperFacade.map(
                customerService.addCustomer(
                        mapperFacade.map(customerDTO,
                                Customer.class)),
                CustomerDTO.class);
    }


}
