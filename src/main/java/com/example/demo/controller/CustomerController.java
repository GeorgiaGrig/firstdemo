package com.example.demo.controller;

import com.example.demo.model.DTO.CustomerDTO;
import org.springframework.http.ResponseEntity;

public interface CustomerController {

    CustomerDTO getCustomerByID(String id);

    CustomerDTO updateCustomer(CustomerDTO customerDTO);

    CustomerDTO addCustomer(CustomerDTO customerDTO);

}
