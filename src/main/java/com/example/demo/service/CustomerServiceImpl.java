package com.example.demo.service;

import com.example.demo.exception.CustomerNotFoundException;
import com.example.demo.model.entity.Customer;
import com.example.demo.repository.CustomerRepository;


public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public Customer getCustomerById(String id)  {
        if(!customerRepository.exists(id)){
            throw new CustomerNotFoundException();
        }
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        if(!customerRepository.exists(customer.getId())){
            throw new CustomerNotFoundException();
        }
        customerRepository.delete(customer.getId());
        return customerRepository.save(customer);
    }

    @Override
    public Customer addCustomer(Customer customer) {

       return customerRepository.save(customer);
    }

}
