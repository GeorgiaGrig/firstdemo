package com.example.demo.service;

import com.example.demo.exception.CustomerNotFoundException;
import com.example.demo.exception.OrderNotFoundException;
import com.example.demo.model.entity.Order;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.OrderRepository;

import java.util.List;

public class OrderServiceImpl implements  OrderService{

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    public OrderServiceImpl(OrderRepository orderRepository, CustomerRepository customerRepository) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Order> getOrderByCustomerId(String id) {
        if(!customerRepository.exists(id)) throw new CustomerNotFoundException();
        return orderRepository.getOrdersByCustomerID(id);
    }

    @Override
    public Order addOrder(Order order) {
        if(!customerRepository.exists(order.getCustomerID())) throw new CustomerNotFoundException();
        return orderRepository.save(order);
    }

    @Override
    public Order deleteOrder(String id) {
        if(!orderRepository.exists(id)){
            throw new OrderNotFoundException();
        }
        Order order = orderRepository.getOrderById(id);
        orderRepository.delete(id);
        return order;
    }

    @Override
    public Order getOrderById(String id) {
        if(!orderRepository.exists(id)){
            throw new OrderNotFoundException();
        }
        return orderRepository.getOrderById(id);
    }
}
