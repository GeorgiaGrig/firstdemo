package com.example.demo.service;

import com.example.demo.model.entity.Customer;

public interface CustomerService {

    Customer getCustomerById(String id);

    Customer updateCustomer(Customer customer);

    Customer addCustomer(Customer customer);
}
