package com.example.demo.service;

import com.example.demo.model.entity.Order;

import java.util.List;

public interface OrderService {

     List<Order> getOrderByCustomerId(String id);

     Order addOrder(Order order);

     Order deleteOrder(String id);

     Order getOrderById(String id);

}
