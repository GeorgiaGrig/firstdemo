package com.example.demo.model.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Getter
@Setter
@ToString
public class Order {

    @Id
    @Generated(value = {})
    private String id;
    @NotNull
    private String customerID;
    private long createdAt;
    private String info;

    public Order() {
    }


    public Order(String id, String customerID, long createdAt, String info) {
        this.id = id;
        this.customerID = customerID;
        this.createdAt = createdAt;
        this.info = info;
    }
}
