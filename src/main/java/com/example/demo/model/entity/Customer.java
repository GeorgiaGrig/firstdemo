package com.example.demo.model.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import javax.annotation.Generated;


@Getter
@Setter
@ToString
public class Customer {

    @Id
    @Generated(value = {})
    private String id;
    private String firstName;
    private String lastName;
    private float balance;

    public Customer() {
    }

    public Customer(String id, String firstName, String lastName, float balance) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
    }
}
