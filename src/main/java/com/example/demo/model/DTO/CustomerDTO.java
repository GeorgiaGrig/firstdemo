package com.example.demo.model.DTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CustomerDTO {
    private String id;
    private String firstName;
    private String lastName;
    private float balance;

    public CustomerDTO() {
    }


    public CustomerDTO(String id, String firstName, String lastName, float balance) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
    }
}
