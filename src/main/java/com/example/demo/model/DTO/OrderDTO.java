package com.example.demo.model.DTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.OffsetDateTime;


@Getter
@Setter
@ToString
public class OrderDTO {
    private String id;
    private String customerID;
    private long createdAt;
    private String info;

    public OrderDTO() {
    }


    public OrderDTO(String id, String customerID, long createdAt, String info) {
        this.id = id;
        this.customerID = customerID;
        this.createdAt = createdAt;
        this.info = info;
    }
}
