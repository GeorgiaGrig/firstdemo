package com.example.demo.repository;

import com.example.demo.model.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OrderRepository extends MongoRepository<Order, String> {
    List<Order> getOrdersByCustomerID(String customerID);

    Order getOrderById(String id);
}
