package com.example.demo.repository;

import com.example.demo.model.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    Customer getCustomerById(String id);

}
