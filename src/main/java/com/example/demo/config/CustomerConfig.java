package com.example.demo.config;

import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.CustomerService;
import com.example.demo.service.CustomerServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
public class CustomerConfig {

    @Bean
    public CustomerService customerService(CustomerRepository customerRepository){
        return new CustomerServiceImpl(customerRepository);
    }

}
