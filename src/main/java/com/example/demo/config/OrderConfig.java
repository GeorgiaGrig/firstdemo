package com.example.demo.config;

import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.OrderRepository;
import com.example.demo.service.OrderService;
import com.example.demo.service.OrderServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderConfig {


    @Bean
    public OrderService orderService(OrderRepository orderRepository, CustomerRepository customerRepository){
        return new OrderServiceImpl(orderRepository, customerRepository);
    }
}
