//package com.example.demo.controller;
//
//import com.example.demo.DemoApplication;
//import com.example.demo.service.OrderServiceImpl;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import java.nio.charset.Charset;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = DemoApplication.class)
//public class OrderControllerTest {
//
//
//    @Mock
//    private OrderServiceImpl orderService;
//
//
//    @InjectMocks
//    private OrderControllerImpl orderController;
//
//    private MockMvc mockMvc;
//
//    private MediaType applicationJsonMediaType = new MediaType(
//            MediaType.APPLICATION_JSON.getType(),
//            MediaType.APPLICATION_JSON.getSubtype(),
//            Charset.forName("utf8")
//    );
//
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        this.mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
//    }
//
//    @Test
//    public void getOrdersByCustomerIdSuccess() throws Exception{}
//
//
//    @Test
//    public void getOrdersByCustomerIdFailNullId() throws Exception{}
//
//
//    @Test
//    public void addOrderSuccess() throws Exception{}
//
//    @Test
//    public void addOrderFailNullCustomerId() throws Exception{}
//
//    @Test
//    public void addOrderFailWrontTypeParamaters() throws Exception{}
//
//    @Test
//    public void deleteOrderSuccess() throws Exception{}
//
//    @Test
//    public void deleteOrderFailNullId() throws Exception{}
//
//    @Test
//    public void getOrderSuccess() throws Exception{}
//
//    @Test
//    public void getOrderFailNullId() throws Exception{}
//}
