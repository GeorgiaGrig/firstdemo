package com.example.demo.repository;

import com.example.demo.model.entity.Customer;
import com.example.demo.model.entity.Order;
import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.core.LoadStrategyEnum;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.util.List;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;

@ContextConfiguration(classes=TestMongoConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class OrderRepositoryTest {

    @Rule
    public MongoDbRule managedMongoDbRule =  newMongoDbRule().defaultSpringMongoDb("test");

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private OrderRepository orderRepository;

    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getOrdersByCustomerIDSuccess() throws Exception{

        String customerID = "1010";

        List<Order> result = orderRepository.getOrdersByCustomerID(customerID);
        Assert.assertEquals(result.size(),4 );
        for(Order order : result){
            Assert.assertEquals(order.getCustomerID(), customerID);
        }

    }


    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getOrdersByCustomerIdNoneFound() throws Exception{
        String customerID = "0000";

        List<Order> result = orderRepository.getOrdersByCustomerID(customerID);

        Assert.assertEquals(result.size(), 0);
    }


    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getOrdersByIdSuccess() throws Exception{
        String id = "45875";
        Order result = orderRepository.getOrderById(id);

        Assert.assertEquals(result.getId(), id);
        Assert.assertEquals(result.getCustomerID(), "1010");
        Assert.assertEquals(result.getCreatedAt(), 1522540);
        Assert.assertEquals(result.getInfo(), "Hello");
    }

    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getOrdersByIdNullId() throws Exception{
        Order result = orderRepository.getOrderById(null);
        Assert.assertNull(result);
    }

    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getOrdersByIdDoesNotExist() throws Exception{
        Order result = orderRepository.getOrderById("123");
        Assert.assertNull(result);
    }

    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void addOrderGenerateId() throws Exception{
        String customerID = "0065";
        long createdAt = 839483284;
        String info = "Info";

        Order order = new Order(null, customerID,createdAt,info);
        Order result = orderRepository.save(order);

        Assert.assertNotNull(result.getId());
        Assert.assertEquals(customerID, result.getCustomerID());
        Assert.assertEquals(createdAt, result.getCreatedAt());
        Assert.assertEquals(info, result.getInfo());
    }


    @Test
    @UsingDataSet(locations="order-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void deleteOrder() throws Exception{
        String id = "14875";
        orderRepository.delete(id);
        Order order = orderRepository.findOne(id);
        Assert.assertNull(order);
    }
}
