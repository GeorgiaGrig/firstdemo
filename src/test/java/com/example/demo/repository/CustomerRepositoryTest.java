package com.example.demo.repository;

import com.example.demo.model.entity.Customer;
import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.core.LoadStrategyEnum;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;

@ContextConfiguration(classes=TestMongoConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerRepositoryTest {


    @Rule
    public MongoDbRule managedMongoDbRule =  newMongoDbRule().defaultSpringMongoDb("test");

    @Inject
    private ApplicationContext applicationContext;

    @Inject
    private CustomerRepository customerRepository;


    @Test
    @UsingDataSet(locations="customer-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getCustomerByIdSuccess() {

        Customer customer = customerRepository.getCustomerById("1010");

        Assert.assertEquals(customer.getId(), "1010");
        Assert.assertEquals(customer.getFirstName(), "John");
        Assert.assertEquals(customer.getLastName(), "Doe");
        Assert.assertEquals(customer.getBalance(), 100, 0);

    }

    @Test
    @UsingDataSet(locations="customer-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getCustomerByIdNullId() throws Exception{
        Customer result = customerRepository.getCustomerById(null);

        Assert.assertNull(result);
    }

    @Test
    @UsingDataSet(locations="customer-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void getCustomerByIdNotFound() throws Exception{
        Customer result = customerRepository.getCustomerById("000");
        Assert.assertNull(result);
    }

    @Test
    @UsingDataSet(locations="customer-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void addCostumerGenerateId() throws Exception{
        String firstName = "Dee";
        String lastName  = "Cooper";
        long balance = 100;

        Customer customer =  new Customer(null, firstName, lastName,balance);

        Customer result = customerRepository.save(customer);

        Assert.assertNotNull(result.getId());
    }

    @Test
    @UsingDataSet(locations="customer-data.json", loadStrategy= LoadStrategyEnum.CLEAN_INSERT)
    public void updateCostumer() throws Exception{
        String id  = "0065";
        String firstName = "Alice";
        String lastName  = "Jett";
        long balance = 100;

        Customer customer =  new Customer(id, firstName, lastName,balance);

        Customer result = customerRepository.save(customer);

        Assert.assertEquals(id, result.getId());
        Assert.assertEquals(firstName, result.getFirstName());
        Assert.assertEquals(lastName, result.getLastName());
        Assert.assertEquals(balance, result.getBalance(), 0);
    }
}
