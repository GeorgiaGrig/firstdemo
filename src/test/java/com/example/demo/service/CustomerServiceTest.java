package com.example.demo.service;

import com.example.demo.exception.CustomerNotFoundException;
import com.example.demo.model.entity.Customer;
import com.example.demo.repository.CustomerRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CustomerServiceTest.TestConfiguration.class)
public class CustomerServiceTest {

    public static class TestConfiguration {

        @Bean
        public CustomerService customerService(CustomerRepository customerRepository) {
            return new CustomerServiceImpl(customerRepository);
        }

        @Bean
        public CustomerRepository customerRepository() {
            return mock(CustomerRepository.class);
        }
    }

    private MockMvc mockMvc;

    @Autowired
    private CustomerServiceImpl customerService;

    @Autowired
    private CustomerRepository customerRepository;



    private String id = "124";
    private String firstName = "John";
    private String lastName = "Doe";
    private long balance = 100;

    private Customer customer;

    @Before
    public void setUp() throws Exception {

        customer = new Customer(
                id,
                firstName,
                lastName,
                balance
        );

        reset(customerRepository);
    }


    @After
    public void tearDown() throws Exception {
       verifyNoMoreInteractions(customerRepository);
    }


    @Test
    public void getCustomerByIdSuccess() throws Exception{

        Mockito.when(customerRepository.exists(id)).thenReturn(true);
        Mockito.when(customerRepository.getCustomerById(id)).thenReturn(customer);

        Customer result = customerService.getCustomerById(id);


        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).getCustomerById(Mockito.anyString());

        Assert.assertEquals(customer, result);

    }

    @Test(expected = CustomerNotFoundException.class)
    public void getCustomerByIdFailCustomerNotFound() throws Exception{
        Mockito.when(customerRepository.exists(id)).thenReturn(false);
        Mockito.when(customerRepository.getCustomerById(id)).thenReturn(customer);

        try {
            customerService.getCustomerById(id);
        } finally {
            Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
            Mockito.verify(customerRepository, VerificationModeFactory.times(0)).getCustomerById(Mockito.anyString());
        }
    }

    @Test
    public void updateCustomerSuccess() throws Exception{
        Mockito.when(customerRepository.exists(id)).thenReturn(true);
        Mockito.when(customerRepository.save(customer)).thenReturn(customer);

        Customer result = customerService.updateCustomer(customer);


        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).delete(Mockito.anyString());
        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).save(Mockito.any(Customer.class));

        Assert.assertEquals(customer, result);

    }

    @Test(expected = CustomerNotFoundException.class)
    public void updateCustomerFailCustomerNotFound() throws Exception{
        Mockito.when(customerRepository.exists(id)).thenReturn(false);

        try {
            customerService.updateCustomer(customer);
        }finally {
            Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
            Mockito.verify(customerRepository, VerificationModeFactory.times(0)).delete(Mockito.anyString());
            Mockito.verify(customerRepository, VerificationModeFactory.times(0)).save(Mockito.any(Customer.class));
        }
    }

    @Test
    public void addCustomerSuccess() throws Exception{
        Mockito.when(customerRepository.save(customer)).thenReturn(customer);

        Customer result = customerService.addCustomer(customer);

        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).save(Mockito.any(Customer.class));

        Assert.assertEquals(customer,result);
    }

}
