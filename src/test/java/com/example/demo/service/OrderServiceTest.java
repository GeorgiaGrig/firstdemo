package com.example.demo.service;

import com.example.demo.exception.CustomerNotFoundException;
import com.example.demo.exception.OrderNotFoundException;
import com.example.demo.model.entity.Order;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.repository.OrderRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = OrderServiceTest.TestConfiguration.class)
public class OrderServiceTest {


    public static class TestConfiguration {

        @Bean
        public OrderService orderService(OrderRepository orderRepository, CustomerRepository customerRepository) {
            return new OrderServiceImpl(orderRepository, customerRepository);
        }

        @Bean
        public OrderRepository orderRepository() {
            return mock(OrderRepository.class);
        }

        @Bean
        public CustomerRepository customerRepository() {
            return mock(CustomerRepository.class);
        }
    }

    private MockMvc mockMvc;

    @Autowired
    private OrderServiceImpl orderService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;


    private String id = "001012";
    private String customerID = "124";
    private long createdAt = 11022030;
    private String info = "Info about the order";

    private Order order;

    @Before
    public void setUp() throws Exception {

        order = new Order(
                id,
                customerID,
                createdAt,
                info
        );
        reset(customerRepository);
        reset(orderRepository);

    }

    @After
    public void tearDown() throws Exception {
        verifyNoMoreInteractions(customerRepository);
        verifyNoMoreInteractions(orderRepository);

    }

    @Test
    public void getOrdersByCustomerIdSuccess() throws Exception{
        List<Order> orders = new ArrayList<>();
        orders.add(order);
        Mockito.when(customerRepository.exists(customerID)).thenReturn(true);
        Mockito.when(orderRepository.getOrdersByCustomerID(customerID)).thenReturn(orders);

        List<Order> result = orderService.getOrderByCustomerId(customerID);

        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).getOrdersByCustomerID(Mockito.anyString());

        Assert.assertEquals(orders, result);
    }

    @Test
    public void getOrdersByCustomerIdEmpty() throws Exception{
        List<Order> orders = new ArrayList<>();
        Mockito.when(customerRepository.exists(customerID)).thenReturn(true);
        Mockito.when(orderRepository.getOrdersByCustomerID(customerID)).thenReturn(orders);

        List<Order> result = orderService.getOrderByCustomerId(customerID);

        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).getOrdersByCustomerID(Mockito.anyString());

        Assert.assertTrue(result.isEmpty());
    }

    @Test(expected = CustomerNotFoundException.class)
    public void getOrdersByCustomerIdFailCustomerNotFound() throws Exception{

        Mockito.when(customerRepository.exists(customerID)).thenReturn(false);

        try {
            orderService.getOrderByCustomerId(customerID);
        } finally {
            Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
            Mockito.verify(orderRepository, VerificationModeFactory.times(0)).getOrdersByCustomerID(Mockito.anyString());
        }
 }

    @Test
    public void addOrderSuccess() throws Exception{
        Mockito.when(customerRepository.exists(customerID)).thenReturn(true);
        Mockito.when(orderRepository.save(order)).thenReturn(order);

        Order result = orderService.addOrder(order);

        Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).save(Mockito.any(Order.class));

        Assert.assertEquals(order, result);
    }

    @Test(expected = CustomerNotFoundException.class)
    public void addOrderFailCustomerNotFound() throws Exception{
        Mockito.when(customerRepository.exists(customerID)).thenReturn(false);

        try {
            orderService.addOrder(order);
        } finally {
            Mockito.verify(customerRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
            Mockito.verify(orderRepository, VerificationModeFactory.times(0)).save(Mockito.any(Order.class));
        }
    }


    @Test
    public void deleteOrderSuccess() throws Exception{
        Mockito.when(orderRepository.exists(id)).thenReturn(true);
        Mockito.when(orderRepository.getOrderById(id)).thenReturn(order);

        Order result = orderService.deleteOrder(id);

        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).getOrderById(Mockito.anyString());
        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).delete(Mockito.anyString());

        Assert.assertEquals(order, result);
    }

    @Test(expected = OrderNotFoundException.class)
    public void deleteOrderFailIDNOtFound() throws Exception{
        Mockito.when(orderRepository.exists(id)).thenReturn(false);

        try {
            orderService.deleteOrder(id);
        } finally {
            Mockito.verify(orderRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
            Mockito.verify(orderRepository, VerificationModeFactory.times(0)).getOrderById(Mockito.anyString());
            Mockito.verify(orderRepository, VerificationModeFactory.times(0)).delete(Mockito.anyString());
        }
    }

    @Test
    public void getOrderByIdSuccess() throws Exception{
        Mockito.when(orderRepository.exists(id)).thenReturn(true);
        Mockito.when(orderRepository.getOrderById(id)).thenReturn(order);

        Order result = orderService.getOrderById(id);

        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
        Mockito.verify(orderRepository, VerificationModeFactory.times(1)).getOrderById(Mockito.anyString());

        Assert.assertEquals(order, result);
    }

    @Test(expected = OrderNotFoundException.class)
    public void getOrderByIdFailIDNOtFound() throws Exception{
        Mockito.when(orderRepository.exists(id)).thenReturn(false);

        try {
            orderService.getOrderById(id);
        } finally {
            Mockito.verify(orderRepository, VerificationModeFactory.times(1)).exists(Mockito.anyString());
            Mockito.verify(orderRepository, VerificationModeFactory.times(0)).getOrderById(Mockito.anyString());
        }
    }

}
